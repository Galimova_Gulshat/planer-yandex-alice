﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlanerDatabase
{
    class DbConnection 
    {
        public static void AddUser(User user)
        {
            using (DataContext db = new DataContext())
            {
                if (!db.Users.Where(u => u.AliceClientId == user.AliceClientId).Any())
                {
                    db.Users.Add(user);
                    db.States.Add(new State() { UserId = user.Id });
                }
                db.SaveChanges();
            }
        }

        public static int FindUserId(string clientId)
        {
            using (DataContext db = new DataContext())
            {
                var us = db.Users.FirstOrDefault(u => u.AliceClientId == clientId);
                if (us == default(User))
                    return -1;
                return us.Id;
            }
        }

        public static void AddTask(Task task)
        {
            using (DataContext db = new DataContext())
            {
                var id = task.UserId;
                if (!db.Tasks.Where(t => t.Name == task.Name && t.Term == task.Term).Any())
                {
                    db.Tasks.Add(task);
                    db.SaveChanges();
                }
                var state = db.States.First(s => s.UserId == id);
                state.TaskId = db.Tasks.First(t => t.Name == task.Name && t.Term == task.Term).Id;
                state.TaskSaved = true;
                db.States.Update(state);
                db.SaveChanges();
            }
        }

        #region CompleteTask

        private static void CompleteTask(int userId, Action<Task> setTaskVakue, Action<State> setStateValue)
        {
            using (DataContext db = new DataContext())
            {
                var task = db.Tasks.First(t => t.UserId == userId);
                setTaskVakue(task);
                db.Tasks.Update(task);
                var state = db.States.First(s => s.UserId == userId);
                setStateValue(state);
                db.States.Update(state);
                db.SaveChanges();
            }
        }

        public static void AddTaskCategory(int userId, int category) =>
            CompleteTask(userId, t => t.Category = category, s => s.AddCategory = false);

        public static void AddTaskComplexity(int userId, int complexity) =>
            CompleteTask(userId, t => t.Complexity = complexity, s => s.AddComplexity = false);

        public static void AddTaskDescription(int userId, string description) =>
            CompleteTask(userId, t => t.Description = description, s => s.AddDescription = false);

        public static void AddTaskStatus(int userId, int status) =>
            CompleteTask(userId, t => t.Status = status, s => s.AddStatus = false);

        public static void AddTaskParticipants(int userId, string[] participants) =>
            CompleteTask(userId, t => t.Participants = participants, s => s.AddParticipant = false);

        public static void StopComleteTask(int stateId)
        {
            using (DataContext db = new DataContext())
            {
                var state = db.States.First(s => s.Id == stateId);
                state.TaskId = -1;
                state.TaskSaved = false;
                state.AddCategory = false;
                state.AddComplexity = false;
                state.AddDescription = false;
                state.AddStatus = false;
                state.AddParticipant = false;
                db.States.Update(state);
                db.SaveChanges();
            }
        }

        #endregion

        public static void ClearTasks(string userId, int day)
        {
            using (DataContext db = new DataContext())
            {
                var user = db.Users.First(u => u.AliceClientId == userId).Id;
                var tasks = db.Tasks.Where(t => t.UserId == user && t.Term.CompareTo(DateTime.Today.AddDays(-day)) == -1);
                foreach (var tsk in tasks)
                    db.Tasks.Remove(tsk);
                db.SaveChanges();
            }
        }

        #region TasksList
        public static Task[] GetTasksByDate(string userId, DateTime date) =>
            FindTasks(userId, t => t.Term == date);

        public static Task[] GetTasksByCategory(string userId, int category) =>
            FindTasks(userId, t => t.Category == category);


        public static Task[] GetTasksByStatus(string userId, int status) =>
            FindTasks(userId, t => t.Status == status);

        private static Task[] FindTasks(string userId, Func<Task, bool> func)
        {
            using (DataContext db = new DataContext())
            {
                var user = db.Users.First(u => u.AliceClientId == userId).Id;
                var tasks = db.Tasks.Where(t => t.UserId == user && func(t));
                if (tasks.Count() == 0)
                    return null;
                return tasks.OrderBy(t => t.Complexity).ToArray();
            }
        }

        #endregion

        public static State GetState(string userId)
        {
            using (DataContext db = new DataContext())
            {
                var user = db.Users.First(u => u.AliceClientId == userId).Id;
                return db.States.First(s => s.UserId == user);
            }
        }

        #region ChangeStates

        public static void AddStateCategory(int stateId) =>
            AddStateValue(stateId, s => s.AddCategory = true);

        public static void AddStateComplexity(int stateId) =>
            AddStateValue(stateId, s => s.AddComplexity = true);

        public static void AddStateDescription(int stateId) =>
            AddStateValue(stateId, s => s.AddDescription = true);

        public static void AddStateParticipants(int stateId) =>
            AddStateValue(stateId, s => s.AddParticipant = true);

        public static void AddStateStatus(int stateId) =>
            AddStateValue(stateId, s => s.AddStatus = true);

        public static void WantUpdtaeTsk(int stateId) =>
            AddStateValue(stateId, s => s.TaskSaved = true);

        private static void AddStateValue(int stateId, Action<State> addValue)
        {
            using (DataContext db = new DataContext())
            {
                var state = db.States.First(s => s.Id == stateId);
                addValue(state);
                db.States.Update(state);
                db.SaveChanges();
            }
        }
        #endregion

        public static Task GetTaskInfo(string taskName, string userId)
        {
            using (DataContext db = new DataContext())
            {
                var user = db.Users.First(u => u.AliceClientId == userId).Id;
                return db.Tasks.Where(t => t.UserId == user && t.Name == taskName).Last();
            }
        }
    }
}
