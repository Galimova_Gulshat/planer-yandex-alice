﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PlanerDomain;

namespace PlanerDatabase
{
    class MapEntity
    {
        public static User MapUser(Client user) =>
            new User() { AliceClientId = user.ClientId };

        public static PlanerDomain.State MapState(State state)
        {
            var Wstate = new PlanerDomain.State(state.Id, state.TaskId, state.UserId);
            if (state.TaskSaved)
                Wstate.SaveTask();
            Wstate.SetAddValues(state.AddCategory, state.AddComplexity, state.AddDescription, state.AddStatus, state.AddParticipant);
            if (state.End)
                Wstate.SetEnd();
            return Wstate;
        }

        public static Task MapTask(Goal task) =>
            new Task()
            {
                Name = task.Name,
                Term = task.Term,
                UserId = task.UserId,
                Category = (int)task.Category,
                Complexity = (int)task.Complexity,
                Description = task.Description,
                Status = (int)task.Status,
                Participants = task.Participants.Select(p => p.ToString()).ToArray()
            };

        public static Goal MapTaskToGoal(Task task) =>
            new Goal(task.Id, task.Name, task.Term, task.UserId, (Status)task.Status, (Complexities)task.Complexity, (Categories)task.Category, task.Description,
                task.Participants.ToList());
    }
}

