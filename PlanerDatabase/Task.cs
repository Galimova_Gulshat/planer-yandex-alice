﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlanerDatabase
{
    class Task
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Term { get; set; }
        public int UserId { get; set; }

        public string[] Participants { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public int Complexity { get; set; }
        public int TaskId { get; set; }
        public int Category { get; set; }
    }
}
