﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace PlanerDatabase
{
    class DataContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<State> States { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=ec2-54-217-235-16.eu-west-1.compute.amazonaws.com;Port=5432;Database=d1344mkqg70oa1;Username=buoywroymhftsq;Password=df16c5d820920149ddb4434c5ad6b54565f37275dcfc2f443907ab71d9cf8ac6;SslMode=Require;Trust Server Certificate=True");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasIndex(u => u.AliceClientId);
            modelBuilder.Entity<Task>().HasIndex(t => t.UserId);
            modelBuilder.Entity<Task>().HasIndex(t => new { t.Term, t.Name });
            modelBuilder.Entity<Task>().HasIndex(t => t.Category);
            modelBuilder.Entity<Task>().HasIndex(t => t.Status);
        }
    }
}
