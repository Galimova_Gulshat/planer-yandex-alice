﻿using PlanerDomain;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace PlanerDatabase
{
    public static class SetData
    {
        public static void AddUser(Client user) =>
            DbConnection.AddUser(MapEntity.MapUser(user));

        #region TasksList
        public static string[] GetTasksByDate(string clientId, DateTime date)
        {
            var tasks = DbConnection.GetTasksByDate(clientId, date);
            return GetTasksList(tasks);
        }

        public static string[] GetTasksByCategory(string clientId, Categories category)
        {

            var tasks = DbConnection.GetTasksByCategory(clientId, (int)category);
            return GetTasksList(tasks);
        }

        public static string[] GetTasksByStatus(string clientId, Status status)
        {
            var tasks = DbConnection.GetTasksByStatus(clientId, (int)status);
            return GetTasksList(tasks);
        }

        private static string[] GetTasksList(Task[] tasks)
        {
            if (tasks == null)
                return new string[] { };
            return tasks.Select(t => t.Name).ToArray();
        }
        #endregion

        public static void ClearTasks(string clientId, int day) =>
            DbConnection.ClearTasks(clientId, day);

        #region AddAndCompleteTask
        public static void AddTask(Goal task) =>
            DbConnection.AddTask(MapEntity.MapTask(task));

        public static void AddCategoryTask(int userId, Categories category) =>
            DbConnection.AddTaskCategory(userId, (int)category);

        public static void AddComplexityTask(int userId, Complexities category) =>
             DbConnection.AddTaskComplexity(userId, (int)category);

        public static void AddTaskDescription(int userId, string category) =>
            DbConnection.AddTaskDescription(userId, category);

        public static void AddTaskStatus(int userId, Status cat) =>
            DbConnection.AddTaskStatus(userId, (int)cat);

        public static void AddTaskParticipants(int userId, string cat) =>
            DbConnection.AddTaskParticipants(userId, cat.Split(","));
        #endregion

        public static PlanerDomain.State GetState(string userId) =>
            MapEntity.MapState(DbConnection.GetState(userId));

        public static int GetUserId(string clientId)
        {
            var id = DbConnection.FindUserId(clientId);
            if (id == -1)
            {
                AddUser(new Client(0, clientId));
                return DbConnection.FindUserId(clientId);
            }
            return id;
        }

        public static Goal GetTaskInfo(string clientId, string taskName)
        {
            var task = DbConnection.GetTaskInfo(taskName, clientId);
            return MapEntity.MapTaskToGoal(task);
            //var partBuilder = new StringBuilder();
            //partBuilder.AppendJoin(',', task.Participants);
            //var builder = new StringBuilder();
            //builder.Append($"Категория: {(Categories)task.Category}\n");
            //builder.Append($"Сложность: {task.Complexity}\n");
            //builder.Append($"Описание: {task.Description}\n");
            //builder.Append($"Участники: {partBuilder.ToString()}\n");
            //builder.Append($"Статус: {(Status)task.Status}\n");
            //builder.Append($"Срок: {task.Term}");
            //return builder.ToString();
        }
    }

    public static class DbExtention
    {
        public static void WantUpdateTask(this PlanerDomain.State state) =>
            DbConnection.WantUpdtaeTsk(state.Id);

        public static void AddCategory(this PlanerDomain.State state) =>
            DbConnection.AddStateCategory(state.Id);

        public static void AddComplexity(this PlanerDomain.State state) =>
            DbConnection.AddStateComplexity(state.Id);

        public static void AddDescription(this PlanerDomain.State state) =>
            DbConnection.AddStateDescription(state.Id);

        public static void AddParticipants(this PlanerDomain.State state) =>
            DbConnection.AddStateParticipants(state.Id);

        public static void AddStatus(this PlanerDomain.State state) =>
            DbConnection.AddStateStatus(state.Id);

        public static void EndSetTask(this PlanerDomain.State state) =>
            DbConnection.StopComleteTask(state.Id);
    }
}
