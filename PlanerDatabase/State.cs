﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlanerDatabase
{
    class State
    {
        public int Id { get; set; }
        public int TaskId { get; set; }
        public int UserId { get; set; }

        public bool TaskSaved { get; set; }

        public bool AddCategory { get; set; }
        public bool AddStatus { get; set; }
        public bool AddParticipant { get; set; }
        public bool AddDescription { get; set; }
        public bool AddComplexity { get; set; }

        public bool End { get; set; }
    }
}
