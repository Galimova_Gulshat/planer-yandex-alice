﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanerDomain
{
    public enum Status
    {
        NotStarted = 1,
        Started = 2,
        Completed = 3
    }

    public enum Categories
    {
        Home = 1,
        Work = 2,
        Study = 3,
        Other = 4,
        People = 5
    }

    public enum Complexities
    {
        VeryEasy = 1,
        Easy = 2,
        Normal = 3,
        Hard = 4,
        VeryHard = 5
    }
}
