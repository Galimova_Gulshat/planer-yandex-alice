﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanerDomain
{
    public class Goal : Entity<int>
    {
        public string Name { get; }
        public DateTime Term { get; private set; }
        public int UserId { get; }

        public List<string> Participants { get; private set; }
        public string Description { get; private set; }
        public Status Status { get; private set; }
        public Complexities Complexity { get; private set; }
        public Categories Category { get; private set; }

        public Goal(int id, string name, DateTime term, int userId, Status status = Status.NotStarted, Complexities complexity = Complexities.VeryEasy,
            Categories category = Categories.Other, string description = "", List<string> participants = null) : base(id)
        {
            UserId = userId;
            Name = name;
            Term = term;
            Status = status;
            Complexity = complexity;
            Category = category;
            Description = description == null ? string.Empty : description;
            Participants = participants == null ? new List<string>() : participants;
        }
    }
}
