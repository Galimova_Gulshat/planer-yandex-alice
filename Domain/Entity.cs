﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanerDomain
{
    public class Entity<TId>
    {
        public Entity(TId id)
        {
            Id = id;
        }

        public TId Id { get; }

        protected bool Equals(Entity<TId> other) =>
            EqualityComparer<TId>.Default.Equals(Id, other.Id);

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Entity<TId>)obj);
        }

        public override int GetHashCode() =>
            EqualityComparer<TId>.Default.GetHashCode(Id);

        public override string ToString() =>
            $"{GetType().Name}({nameof(Id)}: {Id})";
    }
}
