﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanerDomain
{
    public class Client : Entity<int>
    {
        public string ClientId { get; }

        public Client(int id, string clientId) : base(id)
        {
            ClientId = clientId;
        }
    }
}
