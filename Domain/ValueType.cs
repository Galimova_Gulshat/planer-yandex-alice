﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanerDomain
{
    public class ValueType<T>
    {
        public override string ToString()
        {
            var resultStr = new StringBuilder();
            var thisProperties = this.GetType().GetProperties();
            foreach (var property in thisProperties)
                resultStr.Append(property.GetValue(this).ToString() + "; ");
            resultStr.Remove(resultStr.Length - 2, 2);
            return resultStr.ToString();
        }

        public override int GetHashCode()
        {
            var thisProperties = this.GetType().GetProperties();
            var hash = 0;
            var index = 111;
            foreach (var property in thisProperties)
            {
                unchecked { hash += property.GetValue(this).GetHashCode() * index; }
                index++;
            }
            return hash;
        }

        public override bool Equals(object obj)
        {
            if (obj == null && this == null)
                return true;
            if (obj == null && this != null)
                return false;
            if (!(obj is T tObj))
                return false;
            return this.Equals(tObj);
        }

        public bool Equals(T tObj)
        {
            if (tObj == null && this != null)
                return false;
            var thisProperties = GetType().GetProperties();
            foreach (var property in thisProperties)
            {
                var value = tObj.GetType().GetProperty(property.Name).GetValue(tObj);
                var thisValue = property.GetValue(this);
                if (value == null && thisValue == null)
                    continue;
                if (value == null && thisValue != null
                    || value != null && thisValue == null)
                    return false;
                if (!value.Equals(thisValue))
                    return false;
            }
            return true;
        }
    }
}
