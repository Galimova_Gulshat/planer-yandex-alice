﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlanerDomain
{
    public class State : Entity<int>
    {
        public int TaskId { get; }
        public int UserId { get; }
        public bool IsWantSetTask { get; private set; }

        public bool AddCategory { get; private set; }
        public bool AddComplexity { get; private set; }
        public bool AddDescription { get; private set; }
        public bool AddStatus { get; private set; }
        public bool AddParticipant { get; private set; }

        public bool End { get; private set; }

        public State(int id, int taskId, int userId) : base(id)
        {
            TaskId = taskId;
            UserId = userId;
        }

        public void SetAddValues(bool cat, bool com, bool desc, bool stat, bool part)
        {
            AddCategory = cat;
            AddComplexity = com;
            AddDescription = desc;
            AddStatus = stat;
            AddParticipant = part;
        }

        public void SaveTask() =>
            IsWantSetTask = true;

        public void SetEnd() =>
            End = true;
    }
}
