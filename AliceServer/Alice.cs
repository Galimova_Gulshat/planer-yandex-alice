﻿using System;
using System.Threading.Tasks;
using AliceListener;
using AliceRequestParser;
using Microsoft.AspNetCore;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Text;

namespace PlanerServer
{
    public class Alice : Controller
    {
        static void Main(string[] args)
        {
            AliceHttpListener.StartListenOnDefaultPort();
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
              .ConfigureServices(srv => srv.AddCors().AddMvc())
              .Configure(app => app.UseCors(options => options.AllowAnyOrigin()
                                                              .AllowAnyMethod()
                                                              .AllowAnyHeader()
                                                              .AllowCredentials()).UseMvc());

        [HttpPost("/alice")]
        public AliceResponse WebHook([FromBody] AliceRequest req)
        {
            return GetResult(req).Result;
            //var str = new Service().GetResponseModel(JsonConvert.SerializeObject(req)).Result;
            //if (str == null)
            //    return req.Reply("Что-то пошло не так");
            //return req.Reply(JsonConvert.DeserializeObject<ResponseModel>(str));
        }

        private static async Task<AliceResponse> GetResult(AliceRequest req)
        {
            return await Task.Run(() =>
            {
                var str = new Service().GetResponseModel(JsonConvert.SerializeObject(req)).Result;
                if (str == null)
                    return req.Reply("Что-то пошло не так");
                return req.Reply(JsonConvert.DeserializeObject<ResponseModel>(str));
            });
        }
    }
}