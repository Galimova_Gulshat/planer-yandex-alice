﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using AliceListener;
using AliceRequestParser;
using Microsoft.AspNetCore;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace PlanerServer
{
    public static class Extensions
    {
        public static AliceResponse Reply(
          this AliceRequest req,
          ResponseModel responseModel) => new AliceResponse
          {
              Response = responseModel,
              Session = req.Session
          };

        public static AliceResponse Reply(
          this AliceRequest req,
          string text,
          bool endSession = false,
          ButtonModel[] buttons = null) => new AliceResponse
          {
              Response = new ResponseModel
              {
                  Text = text,
                  Tts = text,
                  EndSession = endSession
              },
              Session = req.Session
          };

        
    }
}
