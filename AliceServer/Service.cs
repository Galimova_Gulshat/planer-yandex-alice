﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PlanerServer
{
    public class Service
    {
        private string url;

        public Service() =>
            url = $"http://localhost:8080/?AliceRequest=##function##";

        public async Task<string> GetResponseModel(string function)
        {
            HttpWebRequest request = WebRequest.Create(url.Replace("##function##", function)) as HttpWebRequest;

            HttpWebResponse response = await request.GetResponseAsync() as HttpWebResponse;
            WebHeaderCollection header = response.Headers;
            string responseString;
            using (var reader = new System.IO.StreamReader(response.GetResponseStream(), Encoding.UTF8))
                responseString = reader.ReadToEnd();

            return responseString;
        }
    }
}
