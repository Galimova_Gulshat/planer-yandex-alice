﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace PlanerServer
{
    public class SessionModel
    {
        //Признак новой сессии
        [JsonProperty("new")]
        public bool New { get; set; }

        //Уникальный идентификатор сессии, максимум 64 символов
        [JsonProperty("session_id")]
        public string SessionId { get; set; }

        //Идентификатор сообщения в рамках сессии, максимум 8 символов
        [JsonProperty("message_id")]
        public int MessageId { get; set; }

        //Идентификатор вызываемого навыка, присвоенный при создании
        [JsonProperty("skill_id")]
        public string SkillId { get; set; }

        //Идентификатор экземпляра приложения, в котором пользователь общается с Алисой, максимум 64 символа
        [JsonProperty("user_id")]
        public string UserId { get; set; }
    }

    public class ResponseModel
    {
        //Текст, который следует показать и сказать пользователю/ Не пустой
        [JsonProperty("text")]
        public string Text { get; set; }

        //Ответ в формате TTS(text-to-speech), максимум 1024 символа
        [JsonProperty("tts")]
        public string Tts { get; set; }

        //Признак конца разговора
        [JsonProperty("end_session")]
        public bool EndSession { get; set; }

        //Кнопки, которые следует показать пользователю
        [JsonProperty("buttons")]
        public ButtonModel[] Buttons { get; set; }
    }

    public class ButtonModel
    {
        //Текст кнопки, обязателен для каждой кнопки. Максимум 64 символа
        [JsonProperty("title")]
        public string Title { get; set; }

        //Произвольный JSON, который Яндекс.Диалоги должны отправить обработчику, если данная кнопка будет нажата. Максимум 4096 бай
        [JsonProperty("payload")]
        public object Payload { get; set; }

        //URL, который должна открывать кнопка, максимум 1024 байта
        //Если свойство url не указано, по нажатию кнопки навыку будет отправлен текст кнопки
        [JsonProperty("url")]
        public string Url { get; set; }

        //Признак того, что кнопку нужно убрать после следующей реплики пользователя
        [JsonProperty("hide")]
        public bool Hide { get; set; }
    }

    public class MetaModel
    {
        //Язык в POSIX-формате, максимум 64 символа
        [JsonProperty("locale")]
        public string Locale { get; set; }

        //Название часового пояса, включая алиасы, максимум 64 символа
        [JsonProperty("timezone")]
        public string Timezone { get; set; }

        //Идентификатор устройства и приложения, в котором идет разговор, максимум 1024 символа
        [JsonProperty("client_id")]
        public string ClientId { get; set; }
    }

    public class AliceRequest
    {
        //Информация об устройстве, с помощью которого пользователь разговаривает с Алисой
        [JsonProperty("meta")]
        public MetaModel Meta { get; set; }

        [JsonProperty("request")]
        public RequestModel Request { get; set; }

        ////Данные о сессии
        [JsonProperty("session")]
        public SessionModel Session { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }

    public enum RequestType
    {
        SimpleUtterance,
        ButtonPressed
    }

    public class RequestModel
    {
        //Запрос, который был передан вместе с командой активации навыка
        [JsonProperty("command")]
        public string Command { get; set; }

        //Тип ввода, обязательное свойство
        [JsonProperty("type")]
        public RequestType Type { get; set; }

        //Полный текст пользовательского запроса, максимум 1024 символа
        [JsonProperty("original_utterance")]
        public string OriginalUtterance { get; set; }

        //JSON, полученный с нажатой кнопкой от обработчика навыка (в ответе на предыдущий запрос), максимум 4096 байт.
        [JsonProperty("payload")]
        public JObject Payload { get; set; }

        //Слова и именованные сущности, которые Диалоги извлекли из запроса пользователя
        [JsonProperty("nlu")]
        public Nlu Nlu { get; set; }
    }

    public class Nlu
    {
        //Массив слов из произнесенной пользователем фразы
        [JsonProperty("tokens")]
        public string[] Tokens { get; set; }

        //Массив именованных сущностей
        [JsonProperty("entities")]
        public Entities[] Entities { get; set; }
    }

    public class Entities
    {
        //Обозначение начала и конца именованной сущности в массиве слов. Нумерация слов в массиве начинается с 0
        [JsonProperty("tokens")]
        public Tokens Tokens { get; set; }

        //Тип именованной сущности.Возможные значения:
        //YANDEX.DATETIME — дата и время, абсолютные или относительные.
        //YANDEX.FIO — фамилия, имя и отчество.
        //YANDEX.GEO — местоположение(адрес или аэропорт).
        //YANDEX.NUMBER — число, целое или с плавающей точкой.
        [JsonProperty("type")]
        public string Type { get; set; }

        //Формальное описание именованной сущности
        [JsonProperty("value")]
        public IValue Value { get; set; }
    }

    public interface IValue { }

    public class NameValue : IValue
    {
        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("patronymic_name")]
        public string PatronymicName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }
    }

    public class DateValue : IValue
    {
        [JsonProperty("year")]
        public int Year { get; set; }

        [JsonProperty("month")]
        public int Month { get; set; }

        [JsonProperty("day")]
        public int Day { get; set; }

        [JsonProperty("hour")]
        public int Hour { get; set; }

        [JsonProperty("minute")]
        public int Minute { get; set; }
    }

    public class Tokens
    {
        //Первое слово именованной сущности
        [JsonProperty("start")]
        public int Start { get; set; }

        //Первое слово после именованной сущности
        [JsonProperty("end")]
        public int End { get; set; }
    }

    public class AliceResponse
    {
        [JsonProperty("response")]
        public ResponseModel Response { get; set; }

        [JsonProperty("session")]
        public SessionModel Session { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; } = "1.0";
    }
}
