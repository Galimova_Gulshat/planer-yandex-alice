﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using AliceRequestParser;
using System.Linq;
using PlanerDomain;
using PlanerDatabase;
using System.Text.RegularExpressions;
using System.Diagnostics;

namespace AliceListener
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"{DateTime.Today.AddDays(-7)}");
            Console.WriteLine($"{new DateTime(2018, 12, 13).CompareTo(DateTime.Today.AddDays(-7))}");
        }
    }

    public class AliceHttpListener
    {
        private string strError = "Неправильные данные";
        private string url;
        private bool isListening = false;
        private HttpListener listener = new HttpListener();
        private static Encoding win1251;

        public static void StartListen(string port, string host)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            win1251 = Encoding.GetEncoding("Windows-1251");
            var listen = new AliceHttpListener(port, host);
            listen.Start();
        }

        public static void StartListenOnDefaultPort()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            win1251 = Encoding.GetEncoding("Windows-1251");
            var listen = new AliceHttpListener();
            listen.Start();
        }

        public AliceHttpListener(string port, string host) =>
            url = $"http://{port}:{host}/";

        public AliceHttpListener() =>
            url = "http://localhost:8080/";

        public async void Start()
        {
            listener.Prefixes.Add(url);
            listener.Start();

            isListening = true;

            while (isListening)
            {
                HttpListenerContext context = await listener.GetContextAsync();

                HttpListenerRequest request = context.Request;

                HttpListenerResponse response = context.Response;

                if (!isListening)
                    break;
                var func = request.QueryString["AliceRequest"];
                var result = await GetResultAsync(func);

                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);              
                var win1251Buffer = win1251.GetBytes(result);
                var buffer = Encoding.Convert(win1251, Encoding.UTF8, win1251Buffer);

                response.ContentLength64 = buffer.Length;
                response.OutputStream.Write(buffer, 0, buffer.Length);
                response.OutputStream.Close();

                response.StatusCode = (int)HttpStatusCode.OK;
                using (Stream stream = response.OutputStream) { }
            }
        }

        public async Task<string> GetResultAsync(string request)
        {
            if (request == null)
                return strError;
            var responce = new RequestParser().GetResponse(request);
            return await Task.Run(() => responce == null ? strError : responce.ToString());
        }

        public void StopListen()
        {
            isListening = false;
            listener.Stop();
            listener.Close();
        }
    }
}
