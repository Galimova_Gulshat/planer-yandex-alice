﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AliceRequestParser
{
    public static class Extentions
    {
        public static string Reply(
          this AliceRequest req,
          string text,
          bool endSession = false,
          ButtonModel[] buttons = null)
        {
            var butt = new StringBuilder();
            butt.Append("[");
            if (buttons != null)
            {
                foreach (var b in buttons)
                    butt.Append($"{{ \"title\":\"{b.Title}\", \"hide\":{"true"}}},");
                butt.Remove(butt.Length - 1, 1);
            }
            butt.Append("]");
            var r = $"{{ \"text\":\"{text}\", \"tts\":\"{text}\", \"endSession\":{(endSession ? "true" : "false")}, \"buttons\":{butt.ToString()} }}";
            return r;
        }
    }
}
