﻿using PlanerDatabase;
using PlanerDomain;
using System;
using System.Linq;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;


namespace AliceRequestParser
{
    public class RequestParser
    {
        private static string help = "Для записи задачи введи ее в формат:\n \t Задача ... срок 'дата'\n" +
                    "Для получения списка задач введи:\n \t Дай список задач срок 'дата'\n\t" +
                    "\t Дай список задач с категорией 'название кат.'\n\t" +
                    "\t Дай список задач со статусом 'статус'\n\t" +
                    "Для очистки списка задач введи:\n\t Почисти список (укажи количество дней)\n" +
                    "Если при обновлении нажмешь Завершить, не сможешь больше поменять выбранную задачу";

        private static ButtonModel[] taskCompleteButtons = new ButtonModel[] {
                    new ButtonModel() { Title = "Категория", Hide = true },
                    new ButtonModel() { Title = "Сложность", Hide = true },
                    new ButtonModel() { Title = "Описание/уточнение", Hide = true },
                    new ButtonModel() { Title = "Участники", Hide = true },
                    new ButtonModel() { Title = "Завершить", Hide = true }};

        public string GetResponse(string request)
        {
            var req = JsonConvert.DeserializeObject<AliceRequest>(request);
            var command = req.Request.Command;
            if (req.Session.New)
            {
                SetData.AddUser(new Client(0, req.Session.UserId));
                return req.Reply("Скажи мне свои задачи и я их запомню. Для получения инструкции напиши Помощь");
            }
            if (IsMatchCommand(@"^пока", command))
                return req.Reply("До встречи!", true);
            if (IsMatchCommand(@"^помощь", command))
                return req.Reply(help);
            if (IsMatchCommand(@"^дай список", command))
                return GetListOfTasks(req);
            if (IsMatchCommand(@"^задача (.*) срок", command))
                return ParseTaskWithoutAlice(req);
            if (IsMatchCommand(@"^задача (.*)", command))
                return GetTaskInformation(req);
            if (IsMatchCommand(@"^почисти список", command))
            {
                int day = int.Parse(new Regex(@"\d").Match(command).Value);
                SetData.ClearTasks(req.Session.UserId, day);
                return req.Reply("Ваши задачи очищены");
            }
            var state = SetData.GetState(req.Session.UserId);
            if (IsMatchCommand(@"^Изменить статус", command))
                return ChangeStatus(req, state);
            if (state.IsWantSetTask)
                return CompleteTask(req, state);
            return req.Reply("Неопознанный запрос");
        }

        private string GetTaskInformation(AliceRequest req)
        {
            var task = SetData.GetTaskInfo(req.Session.UserId, req.Request.Command);
            var partBuilder = new StringBuilder();
            if (task.Participants != null)
                partBuilder.AppendJoin(',', task.Participants);
            else partBuilder.Append("");
            var builder = new StringBuilder();
            builder.Append($"Категория: {task.Category}\n");
            builder.Append($"Сложность: {task.Complexity}\n");
            builder.Append($"Описание: {task.Description}\n");
            builder.Append($"Участники: {partBuilder.ToString()}\n");
            builder.Append($"Статус: {task.Status}\n");
            builder.Append($"Срок: {task.Term}");
            //return builder.ToString();
            return req.Reply(builder.ToString(), false, new ButtonModel[] { new ButtonModel() { Title = "Изменить статус", Hide = true } });
        }

        private string ChangeStatus(AliceRequest req, State state)
        {
            state.WantUpdateTask();
            state.AddStatus();
            return req.Reply("Выберите статус", false, new ButtonModel[] {
                        new ButtonModel() { Title = "Не начат",  Hide = true },
                        new ButtonModel() { Title = "Завершен",  Hide = true },
                        new ButtonModel() { Title = "В процессе", Hide = true }});
        }

        private bool IsMatchCommand(string commanStart, string command) =>
            new Regex(commanStart, RegexOptions.IgnoreCase).IsMatch(command);


        private string ParseTaskWithoutAlice(AliceRequest req)
        {
            var date = DateTime.Today;
            var parseStr = new string[0];
            var mistake = CheckAndParseTask(req, out date);
            if (!mistake.Equals(String.Empty))
                return req.Reply(mistake);
            var taskName = new Regex(@"задача (.*) срок", RegexOptions.IgnoreCase).Match(req.Request.Command).Value;
            var userId = SetData.GetUserId(req.Session.UserId);
            var newTask = new Goal(0, taskName.Remove(taskName.Length - 4, 4), date, userId);
            SetData.AddTask(newTask);
            return req.Reply("Задача сохранена. Хотите ее дополнить?", false, taskCompleteButtons);
        }

        private string GetListOfTasks(AliceRequest req)
        {
            var tasks = new string[0];
            var error = "Неправильный формат для вывода списка";
            var words = req.Request.Command.Split(' ');
            if (IsMatchCommand(@"категорией", req.Request.Command))
            {
                var cat = MapCategory(words[words.Length - 1]);
                tasks = SetData.GetTasksByCategory(req.Session.UserId, cat);
            }
            else if (IsMatchCommand(@"статусом", req.Request.Command))
            {
                var status = MapStatus(words[words.Length - 1]);
                tasks = SetData.GetTasksByStatus(req.Session.UserId, status);
            }
            else if (IsMatchCommand(@"срок", req.Request.Command))
            {
                var date = DateTime.Today;
                var parseStr = new string[0];
                var mistake = CheckAndParseTask(req, out date);
                if (!mistake.Equals(String.Empty))
                    return req.Reply(mistake);
                tasks = SetData.GetTasksByDate(req.Session.UserId, date);
            }
            if (tasks == null || tasks.Length == 0)
                return req.Reply(error);
            var buttons = new ButtonModel[tasks.Length];
            for (int i = 0; i < tasks.Length; i++)
                buttons[i] = new ButtonModel() { Title = tasks[i], Hide = true };
            return req.Reply("", false, buttons);
        }

        private string CheckAndParseTask(AliceRequest req, out DateTime date)
        {
            date = DateTime.Today;
            var match = new Regex(@"\d*\s*\w*\s?\d*$", RegexOptions.IgnoreCase).Match(req.Request.Command);
            if (match.Success)
                if (!DateTime.TryParse(match.Value, out date))
                    return "Неправильный формат ввода даты";
            return String.Empty;
        }

        private Categories MapCategory(string category)
        {
            var cat = Categories.Other;
            switch (category)
            {
                case "дом":
                    cat = Categories.Home;
                    break;
                case "работа":
                    cat = Categories.Work;
                    break;
                case "люди":
                    cat = Categories.People;
                    break;
                case "учеба":
                    cat = Categories.Study;
                    break;
            }
            return cat;
        }

        private Status MapStatus(string status)
        {
            var stat = Status.NotStarted;
            switch (status)
            {
                case "Звершен":
                    stat = Status.Completed;
                    break;
                case "В процессе":
                    stat = Status.Started;
                    break;
                case "Не начат":
                    stat = Status.NotStarted;
                    break;
            }
            return stat;
        }

        private string CompleteTask(AliceRequest req, State state)
        {
            var command = req.Request.Command;
            switch (command)
            {
                case "Категория":
                    state.AddCategory();
                    return req.Reply("Выберите категорию", false, new ButtonModel[] {
                        new ButtonModel() { Title = "Дом",  Hide = true },
                        new ButtonModel() { Title = "Работа",  Hide = true },
                        new ButtonModel() { Title = "Учеба", Hide = true },
                        new ButtonModel() { Title = "Люди", Hide = true },
                        new ButtonModel() { Title = "Другое", Hide = true }});
                case "Сложность":
                    state.AddComplexity();
                    return req.Reply("Выберите сложность", false, new ButtonModel[] {
                        new ButtonModel() { Title = "1",  Hide = true },
                        new ButtonModel() { Title = "2",  Hide = true },
                        new ButtonModel() { Title = "3", Hide = true },
                        new ButtonModel() { Title = "4", Hide = true },
                        new ButtonModel() { Title = "5", Hide = true }});
                case "Описание/уточнение":
                    state.AddDescription();
                    return req.Reply("Напишите описание");
                case "Участники":
                    state.AddParticipants();
                    return req.Reply("Напишите участников");
                case "Завершить":
                    state.EndSetTask();
                    return req.Reply("Задача обновлена!");
            }
            return CheckState(state, req);
        }

        private string CheckState(State state, AliceRequest req)
        {
            var command = req.Request.Command;
            if (state.AddCategory)
            {
                var category = MapCategory(command.ToLower());
                SetData.AddCategoryTask(state.UserId, category);
                return req.Reply("Категория добавлена", false, new ButtonModel[] { taskCompleteButtons[1], taskCompleteButtons[2], taskCompleteButtons[3], taskCompleteButtons[4] });
            }

            if (state.AddComplexity)
            {
                var complexity = new object();
                if (Enum.TryParse(typeof(Complexities), command, out complexity))
                {
                    SetData.AddComplexityTask(state.UserId, (Complexities)complexity);
                    return req.Reply("Сложность добавлена", false, new ButtonModel[] { taskCompleteButtons[0], taskCompleteButtons[2], taskCompleteButtons[3], taskCompleteButtons[4] });
                }
                return req.Reply("Что-то пошло не так");
            }
            if (state.AddDescription)
            {
                SetData.AddTaskDescription(state.UserId, command);
                return req.Reply("Описание добавлено", false, new ButtonModel[] { taskCompleteButtons[0], taskCompleteButtons[1], taskCompleteButtons[3], taskCompleteButtons[4] });
            }
            if (state.AddParticipant)
            {
                SetData.AddTaskParticipants(state.UserId, command);
                return req.Reply("Участники добавлены", false, new ButtonModel[] { taskCompleteButtons[0], taskCompleteButtons[1], taskCompleteButtons[2], taskCompleteButtons[4] });
            }
            if (state.AddStatus)
            {
                SetData.AddTaskStatus(state.UserId, MapStatus(command));
                return req.Reply("Статус обновлен");
            }
            return req.Reply("Неправильный запрос");
        }
    }
}
