﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AliceRequestParser
{
    public class SessionModel
    {
        //Признак новой сессии
        [JsonProperty("new")]
        public bool New { get; set; }

        //Идентификатор экземпляра приложения, в котором пользователь общается с Алисой, максимум 64 символа
        [JsonProperty("user_id")]
        public string UserId { get; set; }
    }

    public class ResponseModel
    {
        //Текст, который следует показать и сказать пользователю/ Не пустой
        [JsonProperty("text")]
        public string Text { get; set; }

        //Ответ в формате TTS(text-to-speech), максимум 1024 символа
        [JsonProperty("tts")]
        public string Tts { get; set; }

        //Признак конца разговора
        [JsonProperty("end_session")]
        public bool EndSession { get; set; }

        //Кнопки, которые следует показать пользователю
        [JsonProperty("buttons")]
        public ButtonModel[] Buttons { get; set; }
    }

    public class ButtonModel
    {
        //Текст кнопки, обязателен для каждой кнопки. Максимум 64 символа
        [JsonProperty("title")]
        public string Title { get; set; }

        //Признак того, что кнопку нужно убрать после следующей реплики пользователя
        [JsonProperty("hide")]
        public bool Hide { get; set; }
    }

    public class AliceRequest
    {
        [JsonProperty("request")]
        public RequestModel Request { get; set; }

        ////Данные о сессии
        [JsonProperty("session")]
        public SessionModel Session { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }
    }

    public enum RequestType
    {
        SimpleUtterance,
        ButtonPressed
    }

    public class RequestModel
    {
        //Запрос, который был передан вместе с командой активации навыка
        [JsonProperty("command")]
        public string Command { get; set; }

        //Тип ввода, обязательное свойство
        [JsonProperty("type")]
        public RequestType Type { get; set; }

        //JSON, полученный с нажатой кнопкой от обработчика навыка (в ответе на предыдущий запрос), максимум 4096 байт.
        [JsonProperty("payload")]
        public JObject Payload { get; set; }
    }
}
